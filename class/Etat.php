<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Représente l'état d'une fiche de frais
 * 
 * EC = Fiche en cours de saisie
 * ET = Cloturée, en traitement
 * MP = EN paiement
 * VA = Validé
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 * 
 */
class Etat {

    /**
     * Retourne l'identifiant d'un état
     * 
     * @return String un identifiant d'état
     */
    public function getIdEtat() {
        return $this->idEtat;
    }

    /**
     * Retourne le libellé d'un état
     * 
     * @return String Un libellé d'état
     */
    public function getLibelleEtat() {
        return $this->libelleEtat;
    }

    /**
     * Retourne un état de fiche de frais à partir d'un identifiant d'état
     * 
     * @param String $idEtat un identifiant d'état
     * @return Etat|null un état ou null si aucun état n'est identifié par l'identifiant transmis en paramètre
     */
    public static function fetch($idEtat) {
        $etat = null;
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Etat::$selectById);
        $pdoStatement->bindParam(':idEtat', $idEtat);
      $pdoStatement->execute();    
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($record != false) {
            $etat = Etat::arrayToEtat($record);
        }
        unset($pdo);
        return $etat;
    }

    /**
     * Retourne les différents états existant pour une fiche de frais
     * 
     * @return Array une collection des états des fiches de frais
     */
    public static function fetchAll() {
        $etats = array();
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Etat::$select);
       $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $etats[] = Etat::arrayToEtat($record);
        }
        unset($pdo);
        return $etats;
    }

    private static function arrayToEtat(Array $array) {
        $etat = new Etat();
        $etat->idEtat = $array["idEtat"];
        $etat->libelleEtat = $array["libelleEtat"];
        return $etat;
    }

    private $idEtat;
    private $libelleEtat;
    private static $selectById = "select * from etat where idEtat=:idEtat";
    private static $select = "select * from etat";

}
