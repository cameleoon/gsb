<?php

require_once './conf/config.php';

if (isset($_REQUEST)) {
    $idFicheFrais = $_REQUEST["idFicheFrais"];
    $ficheFrais = FicheFrais::fetch($idFicheFrais);
    $ficheFrais->setEtat(Etat::fetch("VA"));
    $ficheFrais->save();
}
header("location: comptable.gestionFicheFrais.php");

